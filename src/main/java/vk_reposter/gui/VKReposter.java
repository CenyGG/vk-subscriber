package vk_reposter.gui;

import org.json.JSONException;
import vk_reposter.service.VkAuthCreation;
import vk_reposter.service.WallReposter;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class VKReposter {
    private JButton startRepostingButton;
    private JPanel panel1;
    private JButton cleanAllRepostsButton;
    private JButton createNewCodeButton;

    public VKReposter(WallReposter wf, VkAuthCreation vkAuth) {
        startRepostingButton.addActionListener(e -> {
            try {
                wf.startReposting();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });
        cleanAllRepostsButton.addActionListener(e -> {
            try {
                wf.clearAll();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });
        createNewCodeButton.addActionListener(e -> {
            try {
                vkAuth.generateAuthCode();
            } catch (JSONException | URISyntaxException | IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    public void startGui(){
        JFrame jfram = new JFrame();
        jfram.setContentPane(this.panel1);
        jfram.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jfram.setPreferredSize(new Dimension(250,250));
        jfram.setLocation(500,200);
        jfram.pack();
        jfram.setVisible(true);
    }
}
