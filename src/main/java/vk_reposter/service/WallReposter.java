package vk_reposter.service;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.wall.Wallpost;
import com.vk.api.sdk.objects.wall.WallpostFull;
import com.vk.api.sdk.objects.wall.responses.GetResponse;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Kamalov on 28.06.2017.
 */
public class WallReposter {
    private VkApiClient vk;
    private UserActor userActor;
    private String[] filters = {
            "жопа",
            "пизда",
            "хуй",
            "секс",
            "sex",
            "пранк",
            "подводим",
            "победители",
            "выиграл",
            "выиграла",
            "поддержите",
            "продается",
            "продам",
            "поздравляем",
            "поздравить"
    };

    private static int MINUTES_BETWEEN_GET_POSTS = 5;
    private static int SECONDS_BETWEEN_REPOST = 20;
    private static int MILISECS_DEFAULT_SLEEP = 334;
    private static String query = "конкурс розыгрыш разыгрывается";

    private Runnable reposter;
    private ArrayBlockingQueue<WallpostFull> toRepost = new ArrayBlockingQueue<>(500);
    private final Object mux = new Object();
    private int TIME_TO_DELETE = 31 * 24 * 60 * 60;

    private final Set<String> alreadyReposted = new HashSet<>();

    public WallReposter(VkAuthCreation vkAuth) throws Exception {
        this.vk = vkAuth.getVkApiClient();
        this.userActor = vkAuth.getUserActor();

        reposter = () -> {
            while (true) {
                synchronized (mux) {
                    if (toRepost.isEmpty())
                        try {
                            mux.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    mux.notify();
                }

                WallpostFull wallpostFull = toRepost.poll();
                try {
                    vk.wall().repost(userActor, "wall" + wallpostFull.getOwnerId() + "_" + wallpostFull.getId()).message("").execute();
                    sleep();
                    vk.groups().join(userActor).groupId(-wallpostFull.getOwnerId()).execute();
                    sleep();
                    TimeUnit.SECONDS.sleep(SECONDS_BETWEEN_REPOST);
                } catch (ApiException | ClientException | InterruptedException e) {
                    e.printStackTrace();
                    try {
                        TimeUnit.SECONDS.sleep(SECONDS_BETWEEN_REPOST);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        };
    }

    private void sleep() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep((int) (MILISECS_DEFAULT_SLEEP + new Random().nextDouble() * 100));
    }

    public void clearAll() throws Exception {
        List<WallpostFull> items;
        do {
            items = vk.wall().get(userActor).execute().getItems();
            sleep();
            for (WallpostFull post : items) {
                vk.wall().delete(userActor).postId(post.getId()).execute();
                sleep();
            }
        } while (!items.isEmpty());
    }

    public void startReposting() throws Exception {
        new Thread(reposter).start();
        proceedOldPosts();

        while (true) {
            List<WallpostFull> wallPosts = getWallPostsToRepost();
            if (!wallPosts.isEmpty()) {
                toRepost.addAll(wallPosts);
                synchronized (mux) {
                    mux.notify();
                }
            }
            TimeUnit.MINUTES.sleep(MINUTES_BETWEEN_GET_POSTS);
        }

    }

    private List<WallpostFull> getWallPostsToRepost() throws Exception {
        List<WallpostFull> giveAway = vk.newsfeed().search(userActor).q(query).count(200).execute().getItems();
        sleep();

        return giveAway.stream()
                .filter((e) -> e.getOwnerId() < 0)
                .filter((e) -> !alreadyReposted.contains(getWallId(e)))
                .filter((e) -> !containsFilter(e.getText()))
                .peek((e) -> alreadyReposted.add(getWallId(e)))
                .collect(Collectors.toList());
    }

    private String getWallId(Wallpost e) {
        return e.getOwnerId().toString() + "_" + e.getId();
    }

    private List<WallpostFull> getWallPastasOnMyPage() throws Exception {
        GetResponse execute = vk.wall().get(userActor).count(100).execute();
        sleep();
        return execute.getItems();
    }

    public void proceedOldPosts() throws Exception {
        List<WallpostFull> wallPostsOnMyPage = getWallPastasOnMyPage();
        List<WallpostFull> deleteOld = wallPostsOnMyPage.stream()
                .filter((e) -> {
                    Wallpost wallpost = e.getCopyHistory().get(0);
                    return (System.currentTimeMillis() / 1000 - wallpost.getDate() > TIME_TO_DELETE);
                }).collect(Collectors.toList());
        for (Wallpost post : deleteOld) {
            deleteWallPost(post);
        }
        List<String> myReposts = wallPostsOnMyPage.stream()
                .map((e) -> e.getCopyHistory().get(0))
                .map((e)->getWallId(e))
                .collect(Collectors.toList());
        alreadyReposted.addAll(myReposts);

    }

    private void deleteWallPost(Wallpost wallPost) throws Exception {
        vk.wall().delete(userActor).postId(wallPost.getId()).execute();
        sleep();
    }

    private boolean containsFilter(String text) {
        text = text.toLowerCase();
        for (String str : filters) {
            if (text.contains(str))
                return true;
        }
        return false;
    }

}
