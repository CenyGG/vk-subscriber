package vk_reposter.service;

public enum VkAuthPermissionsCodes {
    NOTIFY(1),
    FRIENDS(2),
    PHOTOS(4),
    AUDIO(8),
    VIDEO(16),
    PAGES(128),
    SHORT_LINK_TO_APP(256),
    STATUS(1024),
    NOTES(2048),
    MESSAGES(4096),
    WALL(8192),
    ADS(32768),
    OFFLINE(65536),
    DOCS(131072),
    GROUPS(262144),
    NOTIFICATIONS(524288),
    STATS(1048576),
    EMAIL(4194304),
    MARKET(134217728);

    private int value;
    VkAuthPermissionsCodes(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}