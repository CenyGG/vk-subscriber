package vk_reposter.service;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.vk.api.sdk.objects.UserAuthResponse;
import org.json.*;

/**
 * Created by mkamalov on 8/8/2017.
 */
public class VkAuthCreation {
    private VkApiClient vk;

    private String client_id;
    private String client_secret;
    private String display;
    private String redirect_uri;
    private String response_type;
    private String v;
    private String user_code;
    private String user_id;
    private String user_token;

    private Path pathToJSON = Paths.get(VkAuthCreation.class.getClassLoader().getResource("props.json").toURI());
    private JSONObject jsonObj = parseJSONFile();

    public VkAuthCreation() throws JSONException, IOException, URISyntaxException {
        TransportClient transportClient = HttpTransportClient.getInstance();
        vk = new VkApiClient(transportClient);
        parseJSON(jsonObj);
    }

    private void parseJSON(JSONObject obj) throws JSONException, IOException, URISyntaxException {
        client_id = obj.getString("client_id");
        client_secret = obj.getString("client_secret");
        display = obj.getString("display");
        redirect_uri = obj.getString("redirect_uri");
        response_type = obj.getString("response_type");
        v = obj.getString("v");
        user_code = obj.getString("user_code");
        user_id = obj.getString("user_id");
        user_token = obj.getString("user_token");
    }

    private JSONObject parseJSONFile() throws JSONException, IOException, URISyntaxException {
        String content = new String(Files.readAllBytes(pathToJSON));
        return new JSONObject(content);
    }

    public void generateAuthCode() throws JSONException, URISyntaxException, IOException {
        String auhLink = "https://oauth.vk.com/authorize?" +
                "client_id=" + client_id +
                "&display=" + display +
                "&redirect_uri=" + redirect_uri +
                "&scope=" + (VkAuthPermissionsCodes.WALL.getValue() + VkAuthPermissionsCodes.GROUPS.getValue() + VkAuthPermissionsCodes.DOCS.getValue() + VkAuthPermissionsCodes.FRIENDS.getValue() + VkAuthPermissionsCodes.PHOTOS.getValue() + VkAuthPermissionsCodes.VIDEO.getValue()) +
                "&response_type=" + response_type +
                "&v=" + v;

        Desktop.getDesktop().browse(new URI(auhLink));
    }

    /***
     *
     * @return user token and id
     */
    private UserAuthResponse userAuth() throws ClientException, ApiException {
        if (user_code == null || user_code.isEmpty()) {
            System.out.println("You need to set user_code first.\nTo do it please use method generateAuthCode()");
            System.exit(-1);
        }
        return vk.oauth()
                .userAuthorizationCodeFlow(Integer.parseInt(client_id), client_secret, redirect_uri, user_code)
                .execute();
    }

    public UserActor getUserActor() throws Exception {
        UserAuthResponse userAuthResponse;
        if (user_id == null || user_id.isEmpty() || user_token == null || user_id.isEmpty()) {
            userAuthResponse = userAuth();
            jsonObj.put("user_id", userAuthResponse.getUserId());
            jsonObj.put("user_token", userAuthResponse.getAccessToken());
            try (FileWriter file = new FileWriter(pathToJSON.toFile())) {
                file.write(jsonObj.toString());
            }
            return new UserActor(userAuthResponse.getUserId(), userAuthResponse.getAccessToken());
        } else {
            return new UserActor(Integer.parseInt(user_id), user_token);
        }

    }

    public VkApiClient getVkApiClient() {
        return vk;
    }
}
