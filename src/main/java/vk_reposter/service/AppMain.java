package vk_reposter.service;

import vk_reposter.gui.VKReposter;

/**
 * Created by mkamalov on 6/21/2017.
 */
public class AppMain {

    public static void main(String[] args) throws Exception {
        VkAuthCreation vkAuth = new VkAuthCreation();
        //vkAuth.generateAuthCode();

        WallReposter wf = new WallReposter(vkAuth);
        VKReposter gui = new VKReposter(wf, vkAuth);
        gui.startGui();
    }

}
